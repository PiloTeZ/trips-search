<?php

namespace common\models;

use yii\base\Model;

class TripsSearchParams extends Model
{
    public $corporateId;

    public $serviceId;

    public $depAirportId;

    public function rules()
    {
        return [
            [['corporateId', 'serviceId', 'depAirportId'], 'integer'],
        ];
    }
}
