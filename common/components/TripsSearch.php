<?php

namespace common\components;

use common\models\Trip;
use common\models\TripsSearchParams;
use yii\base\BaseObject;
use yii\data\ActiveDataProvider;
use yii\db\Exception;

class TripsSearch extends BaseObject
{
    /**
     * @var TripsSearchParams
     */
    private $searchParams;

    public function __construct(TripsSearchParams $searchParams, array $config = [])
    {
        parent::__construct($config);
        $this->searchParams = $searchParams;
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws Exception
     */
    public function search()
    {
        $query = Trip::find();
        $query->alias('t');

        if (!$this->searchParams->validate()) {
            throw new Exception('Неверные параметры поиска');
        }

        $query->andFilterWhere(['t.corporate_id' => $this->searchParams->corporateId]);

        if ($this->searchParams->serviceId || $this->searchParams->depAirportId) {
            $query
                ->select(['t.*'])
                ->joinWith('flightSegments fs', true, 'RIGHT JOIN')
                ->andFilterWhere(['fs.service_id' => $this->searchParams->serviceId])
                ->andFilterWhere(['fs.depAirportId' => $this->searchParams->depAirportId])
                ->groupBy('t.id');
        }

        return $query;
    }

    /**
     * @return ActiveDataProvider
     * @throws Exception
     */
    public function searchWithProvider()
    {
        return new ActiveDataProvider([
            'query' => $this->search(),
        ]);
    }
}
