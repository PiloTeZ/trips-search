<?php

use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $provider \yii\data\ActiveDataProvider */
/* @var $searchParams \common\models\TripsSearchParams */

$this->title = 'Bad trips';
?>

<?= $this->render('_indexSearchForm', ['searchParams' => $searchParams]) ?>

<div class="site-index">
    <div class="body-content">
        <?= GridView::widget([
            'dataProvider' => $provider,
        ]) ?>
    </div>
</div>
