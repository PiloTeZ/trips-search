<?php

use kartik\select2\Select2;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use yii\web\JsExpression;

/** @var \common\models\TripsSearchParams $searchParams */
$url = \yii\helpers\Url::to(['airports/search-mapped']);
?>

<?php $form = ActiveForm::begin(['id' => 'contact-form', 'action' => '?', 'method' => 'get']); ?>

<?= $form->field($searchParams, 'serviceId') ?>
<?= $form->field($searchParams, 'corporateId') ?>
<?= $form
    ->field($searchParams, 'depAirportId')
    ->widget(Select2::class, [
        'options' => ['placeholder' => 'Search for a airport ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
            ],
            'ajax' => [
                'url' => $url,
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }'),
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(airport) { return airport.text; }'),
            'templateSelection' => new JsExpression('function (airport) { return airport.text; }'),
        ],
    ]) ?>

<div class="form-group">
    <?= Html::submitButton('Найти', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
</div>

<?php ActiveForm::end(); ?>
