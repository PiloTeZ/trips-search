<?php

namespace frontend\controllers;

use common\components\TripsSearch;
use common\models\TripsSearchParams;
use Yii;
use yii\filters\PageCache;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function behaviors()
    {
        $behaviors = [
            'cache' => [
                'class' => PageCache::class,
                'only' => [
                    'index',
                ],
                'variations' => [
                    Yii::$app->request->get(),
                ],
                'duration' => 3600,
            ],
        ];

        return $behaviors;
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        $searchParams = new TripsSearchParams();
        $searchParams->load(Yii::$app->request->get());

        $search = Yii::createObject(TripsSearch::class, [$searchParams]);
        $provider = $search->searchWithProvider();

        return $this->render('index', ['provider' => $provider, 'searchParams' => $searchParams]);
    }
}
