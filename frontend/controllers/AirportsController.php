<?php

namespace frontend\controllers;

use common\components\TripsSearch;
use common\models\AirportName;
use common\models\TripsSearchParams;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

/**
 * Site controller
 */
class AirportsController extends Controller
{
    /**
     * @param $name
     * @return array
     */
    public function actionSearchMapped($q)
    {
        $response = Yii::$app->response;
        $response->format = $response::FORMAT_JSON;

        $airportsQuery = AirportName::find();
        $airportsQuery->andWhere(['like', 'value', $q]);

        $airportNames = $airportsQuery->limit(10)->all();

        $result = [];
        /** @var AirportName $airportName */
        foreach ($airportNames as $airportName) {
            $result[] = ['id' => $airportName->airport_id, 'text' => $airportName->value];
        }

        return ['results' => $result];
    }
}
