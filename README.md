Основной код в:
- common/models
- common/components
- console/migrations

Что бы развернуть, надо:
- выполнить vagrant up
- создать БД
- залить SQL из тестового
- выполнить миграции
