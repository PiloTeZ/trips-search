<?php

use yii\db\Migration;

class m190927_073814_create_flight_services_columns extends Migration
{
    public $db = 'dbCbt';

    public function safeUp()
    {
        $this->createTableLikeOld();
        $this->addColumns();
        $this->addForeignkeys();
        $this->moveData();
        $this->dropOldTable();
        $this->renameNewTable();
    }

    private function createTableLikeOld()
    {
        $this->execute(/** @lang MySQL */
            'CREATE TABLE flight_segment_new LIKE flight_segment');
    }

    private function addColumns()
    {
        $this->addColumn('flight_segment_new', 'trip_id', $this->integer()->after('flight_id'));
        $this->addColumn('flight_segment_new', 'service_id', $this->integer()->after('flight_id'));
    }

    private function addForeignkeys()
    {
        $this->addForeignKey('flight_segment_trip_id_fk', 'flight_segment_new', 'trip_id', 'trip', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('flight_segment_service_id_fk', 'flight_segment_new', 'service_id', 'trip_service', 'id', 'RESTRICT', 'RESTRICT');
    }

    private function moveData()
    {
        $this->execute(/** @lang MySQL */
            'INSERT INTO flight_segment_new (id, flight_id, num, `group`, departureTerminal, arrivalTerminal,
                                flightNumber, departureDate, arrivalDate, stopNumber, flightTime, eTicket, bookingClass,
                                bookingCode, baggageValue, baggageUnit, depAirportId, arrAirportId, opCompanyId,
                                markCompanyId, aircraftId, depCityId, arrCityId, supplierRef, depTimestamp,
                                arrTimestamp, service_id, trip_id)
SELECT flight_segment.*, trip_service.service_id, trip_service.trip_id
FROM flight_segment
       LEFT JOIN trip_service ON trip_service.id = flight_segment.flight_id');
    }

    private function dropOldTable()
    {
        $this->dropTable('flight_segment');
    }

    private function renameNewTable()
    {
        $this->renameTable('flight_segment_new', 'flight_segment');
    }
}
